#include <iostream>
#include <cassert>
#include <vector>

struct Node {
    long long value;
    int id;

    Node(long long v, int i): value(v), id(i) {};
};

class Binary_heap {
    std::vector<int> id_to_pos;
    std::vector<Node> pool;
    int size = 0;

    void sift_up(int i) {
        while (pool[i].value < pool[(i - 1) / 2].value) {
            std::swap(pool[i], pool[(i - 1) / 2]);
            std::swap(id_to_pos[pool[i].id], id_to_pos[pool[(i - 1) / 2].id]);
            i = (i - 1) / 2;
        }
    }

    void sift_down(int i) {
        while (2 * i + 1 < size) {
            int left = 2 * i + 1;
            int right = 2 * i + 2;
            int min_son = left;
            if (right < size && pool[right].value < pool[left].value) {
                min_son = right;
            }
            if (pool[i].value <= pool[min_son].value) {
                break;
            } else {
                std::swap(pool[i], pool[min_son]);
                std::swap(id_to_pos[pool[i].id], id_to_pos[pool[min_son].id]);
                i = min_son;
            }
        }
    }

public:

    Binary_heap (int q) {
        id_to_pos.resize(q + 1);
    }

    long long get_min() {
        assert(size > 0);
        return pool[0].value;
    }

    void extract_min() {
        assert(size > 0);
        pool[0] = pool[size - 1];
        id_to_pos[pool[0].id] = 0;
        sift_down(0);
        pool.pop_back();
        --size;
    }

    void insert(long long x, int id) {
        pool.emplace_back(x, id);
        ++size;
        id_to_pos[id] = size - 1;
        sift_up(size - 1);
    }

    void decrease_key(int id, long long delta) {
        pool[id_to_pos[id]].value -= delta;
        sift_up(id_to_pos[id]);
    }

    bool self_check(int i) {
        if (i > size - 1)
            return true;
        if (id_to_pos[pool[i].id] != i)
            return false;
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        if (left < size && pool[i].value > pool[left].value)
            return false;
        if (right < size && pool[i].value > pool[right].value)
            return false;
        return self_check(left) && self_check(right);
    }
};

void execute(const std::string &type, int cur_id, Binary_heap &heap) {
    using std::cin, std::cout;

    if (type == "insert") {
        long long x;
        cin >> x;
        heap.insert(x, cur_id);

    } else if (type == "getMin") {
        cout << heap.get_min() << '\n';

    } else if (type == "extractMin") {
        heap.extract_min();

    } else if (type == "decreaseKey") {
        int id;
        long long delta;
        cin >> id >> delta;
        heap.decrease_key(id, delta);
    }
}

void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    int q;
    cin >> q;

    Binary_heap heap(q);
    std::string type;
    for (int i = 0; i < q; ++i) {
        cin >> type;
        execute(type, i + 1, heap);
    }

    return 0;
}