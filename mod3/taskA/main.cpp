/* Задача A. AVL-дерево
Реализуйте AVL-дерево. Решения с использованием других структур засчитываться не будут.

* Входные данные
Входной файл содержит описание операций с деревом. Операций не больше 1e5

В каждой строке находится одна из следующих операций:
    insert x
 — добавить в дерево ключ x
    delete x
 — удалить из дерева ключ x. Если ключа x в дереве нет, то ничего делать не надо.
    exists x
 — если ключ x есть в дереве, вывести «true», иначе «false»
    next x
 — минимальный элемент в дереве, больший x, или «none», если такого нет.
    prev x
 — максимальный элемент в дереве, меньший x, или «none», если такого нет.

Все числа во входном файле целые и по модулю не превышают 1e9

* Выходные данные

Выведите последовательно результат выполнения всех операций exists, next, prev.
*/

#include <iostream>

struct Node {
    int key;
    short height = 1;
    Node* left = nullptr;
    Node* right = nullptr;

    explicit Node(int x): key(x) {};

    ~Node() {
        delete left;
        delete right;
    }
};

class AVL_tree {
    Node *root = nullptr;

    static short safe_height(const Node *ptr) {
        if (ptr == nullptr)
            return 0;
        return ptr->height;
    }

    static short balance_factor(const Node *ptr) {
        return safe_height(ptr->right) - safe_height(ptr->left);
    }

    static void fix_height(Node *ptr) {
        ptr->height = std::max(safe_height(ptr->left), safe_height(ptr->right)) + 1;
    }

    static Node *rotate_right(Node *ptr) {
        Node *ptr_l = ptr->left;
        ptr->left = ptr_l->right;
        ptr_l->right = ptr;
        fix_height(ptr);
        fix_height(ptr_l);
        return ptr_l;
    }

    static Node *rotate_left(Node *ptr) {
        Node *ptr_r = ptr->right;
        ptr->right = ptr_r->left;
        ptr_r->left = ptr;
        fix_height(ptr);
        fix_height(ptr_r);
        return ptr_r;
    }

    static Node *balance(Node *ptr) {
        fix_height(ptr);
        if (balance_factor(ptr) == 2) {
            if (balance_factor(ptr->right) < 0) {
                ptr->right = rotate_right(ptr->right);
            }
            return rotate_left(ptr);
        }
        if (balance_factor(ptr) == -2) {
            if (balance_factor(ptr->left) > 0) {
                ptr->left = rotate_left(ptr->left);
            }
            return rotate_right(ptr);
        }
        return ptr;
    }

    static Node* insert(Node* ptr, int k) {
        if (ptr == nullptr) {
            return new Node(k);
        }
        if (ptr->key >= k) {
            ptr->left = insert(ptr->left, k);
        } else {
            ptr->right = insert(ptr->right, k);
        }
        return balance(ptr);
    }

    static Node* find_min(Node* ptr) {
        if (ptr->left != nullptr) {
            return find_min(ptr->left);
        } else {
            return ptr;
        }
    }

    static Node* remove_min(Node* ptr) {
        if (ptr->left == nullptr) {
            return ptr->right;
        }
        ptr->left = remove_min(ptr->left);
        return balance(ptr);
    }

    static Node* remove(Node* ptr, int k) {
        if (ptr == nullptr) return nullptr;
        if (k < ptr->key) {
            ptr->left = remove(ptr->left, k);
        } else if (k > ptr->key) {
            ptr->right = remove(ptr->right, k);
        } else {
            Node *q = ptr->left;
            Node *r = ptr->right;
            delete ptr;
            if (r == nullptr) return q;
            Node *min = find_min(r);
            min->right = remove_min(r);
            min->left = q;
            return balance(min);
        }
        return balance(ptr);
    }

    static bool exists(Node* ptr, int x) {
        if (ptr == nullptr) return false;
        if (ptr->key == x) return true;
        if (ptr->key > x) return exists(ptr->left, x);
        return exists(ptr->right, x);
    }

public:

    AVL_tree() = default;

    void insert(int x) {
        if (!exists(x))
            root = insert(root, x);
    }

    void del(int x) {
        if (exists(x))
            root = remove(root, x);
    }

    bool exists(int x) {
        return exists(root, x);
    }

    int next(int x) {
        Node* ptr = root;

        int c1 = 2e9;
        while (true) {
            if (ptr == nullptr) break;
            if (ptr->key == x) {
                break;
            }
            if (ptr->key > x) {
                c1 = std::min(c1, ptr->key);
                ptr = ptr->left;
            } else {
                ptr = ptr->right;
            }
        }

        int c2 = static_cast<int>(2e9);
        if (ptr != nullptr && ptr->right != nullptr) {
            ptr = ptr->right;
            c2 = ptr->key;
            while (ptr->left != nullptr) {
                ptr = ptr->left;
                c2 = ptr->key;
            }
        }

        return std::min(c1, c2);
    }

    int prev(int x) {
        Node* ptr = root;

        int c1 = -2e9;
        while (true) {
            if (ptr == nullptr) break;
            if (ptr->key == x) {
                break;
            }
            if (ptr->key > x) {
                ptr = ptr->left;
            } else {
                c1 = std::max(c1, ptr->key);
                ptr = ptr->right;
            }
        }

        int c2 = -2e9;
        if (ptr != nullptr && ptr->left != nullptr) {
            ptr = ptr->left;
            c2 = ptr->key;
            while (ptr->right != nullptr) {
                ptr = ptr->right;
                c2 = ptr->key;
            }
        }

        return std::max(c1, c2);
    }

    ~AVL_tree() {
        delete root;
    }
};

inline void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout;
    enable_fast_io();

    AVL_tree tree;
    std::string s;
    int x;
    while (cin >> s >> x) {
        if (s == "break") break;
        if (s == "insert") {
            tree.insert(x);
        } else if (s == "delete") {
            tree.del(x);
        } else if (s == "exists") {
            if (tree.exists(x)) {
                cout << "true\n";
            } else {
                cout << "false\n";
            }
        } else if (s == "next") {
            int res = tree.next(x);
            if (res == 2e9) {
                cout << "none\n";
            } else {
                cout << res << '\n';
            }
        } else if (s == "prev") {
            int res = tree.prev(x);
            if (res == -2e9) {
                cout << "none\n";
            } else {
                cout << res << '\n';
            }
        }
    }

    return 0;
}