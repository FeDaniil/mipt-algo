#include <utility>
#include <cstddef>

template<size_t ChunkSize, size_t Align = alignof(std::max_align_t), size_t StorageSize = (1u << 29u)>
class FixedAllocator {
    using storage_type = std::aligned_storage_t<ChunkSize, Align>;

public:
    FixedAllocator() {
        ++allocators_created;
    };

    int8_t* allocate(size_t n) {
        auto* res = reinterpret_cast<int8_t*>(top);
        top += n;
        return res;
    }

    void deallocate(int8_t* ptr, [[maybe_unused]] size_t n) {
        auto storage_ptr = reinterpret_cast<storage_type*>(ptr);
        if (++storage_ptr == top) {  // if storage_ptr is the last allocated (next is top)
            top = --storage_ptr;  // then move top back to storage_ptr
        }
    }

    ~FixedAllocator() {
        --allocators_created;
        if (allocators_created == 0) {  // if no other allocators to same type exist
            top = pool;  // reset top to pool begin
        }
    }

private:
    inline static storage_type pool[StorageSize / sizeof(storage_type)];
    inline static storage_type* top = pool;
    inline static size_t allocators_created = 0;
};

template<typename T>
class FastAllocator {
    static constexpr bool is_small_T = sizeof(T) <= 32;

public:
    using value_type = T;

    FastAllocator() = default;

    template<typename U>
    FastAllocator(const FastAllocator<U>&) {}

    T* allocate(size_t n) {
        if constexpr (is_small_T) {
            return reinterpret_cast<T*>(fixed_allocator.allocate(n));
        } else {
            return reinterpret_cast<T*>(::operator new(n * sizeof(T)));
        }
    }

    void deallocate(T* ptr, [[maybe_unused]] size_t n) {
        if constexpr (is_small_T) {
            fixed_allocator.deallocate(reinterpret_cast<int8_t*>(ptr), n);
        } else {
            ::operator delete(ptr);
        }
    }

    template<typename ...Args>
    void construct(T* ptr, Args&&... args) {
        new (ptr) T(std::forward<Args>(args)...);
    }

    void destroy(T* ptr) {
        ptr->~T();
    }

private:
    std::conditional_t<is_small_T, FixedAllocator<sizeof(T), alignof(T)>, void*> fixed_allocator;
};

template<typename T, typename Alloc = std::allocator<T>>
class List {
    struct Node;

    template<bool C>
    class common_iterator;

    Alloc alloc;
    using AllocTraits = std::allocator_traits<Alloc>;

    typename AllocTraits::template rebind_alloc<Node> node_alloc;
    using NodeAllocTraits = std::allocator_traits<decltype(node_alloc)>;

public:
    explicit List(const Alloc& alloc = Alloc()): alloc(alloc) {
        init_fictive_nodes();
    }

    List(size_t count, const T& value, const Alloc& alloc = Alloc()): alloc(alloc) {
        init_fictive_nodes();
        for (size_t i = 0; i < count; ++i) {
            push_back(value.value());
        }
    }

    explicit List(size_t count) {
        init_fictive_nodes();
        for (size_t i = 0; i < count; ++i) {
            emplace_back();
        }
    }

    List(const List& list): List(list, AllocTraits::select_on_container_copy_construction(list.get_allocator())) {}

    List(const List& list, const Alloc& alloc): alloc(alloc) {
        init_fictive_nodes();
        for (auto&& x : list) {
            push_back(x);
        }
    }

    List& operator=(const List& list) {
        List copy(list, AllocTraits::propagate_on_container_copy_assignment::value ? list.get_allocator() : alloc);
        swap(copy);
        return *this;
    }

    Alloc get_allocator() const {
        return alloc;
    }

    size_t size() const noexcept {
        return sz;
    }

    using iterator = common_iterator<false>;
    using const_iterator = common_iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    iterator begin() {
        return iterator(fictive_begin->next);
    }

    const_iterator begin() const {
        return cbegin();
    }

    const_iterator cbegin() const {
        return const_iterator(fictive_begin->next);
    }

    iterator end() {
        return iterator(fictive_end);
    }

    const_iterator end() const {
        return cend();
    }

    const_iterator cend() const {
        return const_iterator(fictive_end);
    }

    reverse_iterator rbegin() {
        return reverse_iterator(end());
    }

    const_reverse_iterator rbegin() const {
        return crbegin();
    }

    const_reverse_iterator crbegin() const {
        return const_reverse_iterator(cend());
    }

    reverse_iterator rend() {
        return reverse_iterator(begin());
    }

    const_reverse_iterator rend() const {
        return crend();
    }

    const_reverse_iterator crend() const {
        return const_reverse_iterator(cbegin());
    }

    void push_back(const T& x) {
        emplace(end(), x);
    }

    void push_front(const T& x) {
        emplace(begin(), x);
    }

    void insert(const const_iterator& it, const T& x) {
        emplace(it, x);
    }

    template<typename ...Args>
    void emplace_back(Args&&... args) {
        emplace(end(), std::forward<Args>(args)...);
    }

    template<typename ...Args>
    void emplace_front(Args&&... args) {
        emplace(begin(), std::forward<Args>(args)...);
    }

    template<typename ...Args>
    void emplace(const const_iterator& it, Args&&... args) {
        insert_between(it.node->prev, it.node, make_node(std::forward<Args>(args)...));
        ++sz;
    }

    void pop_back() {
        erase(--end());
    }

    void pop_front() {
        erase(begin());
    }

    void erase(const const_iterator& it) {
        utilize_node(extract(it.node));
        --sz;
    }

    ~List() {
        while (sz != 0) {
            pop_back();
        }
        NodeAllocTraits::deallocate(node_alloc, fictive_end, 1);
        NodeAllocTraits::deallocate(node_alloc, fictive_begin, 1);
    }

private:
    Node* fictive_begin;
    Node* fictive_end;
    size_t sz = 0;

    struct Node {
        Node* prev = nullptr;
        Node* next = nullptr;
        T value = T();

        Node() = default;

        explicit Node(const T& x): value(x) {}
    };

    template<bool C>
    class common_iterator {
    public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = long long;
        using value_type        = std::conditional_t<C, const T, T>;
        using pointer           = std::conditional_t<C, const T*, T*>;
        using reference         = std::conditional_t<C, const T&, T&>;

        common_iterator(Node* x): node(reinterpret_cast<Node*>(x)) {}

        common_iterator(const common_iterator& other): node(other.node) {}

        template<bool U = C, std::enable_if_t<U, bool> = true>
        common_iterator(const iterator& iter): node(iter.node) {}

        common_iterator& operator=(common_iterator other) {
            swap(other);
            return *this;
        }

        common_iterator& operator++() {
            node = node->next;
            return *this;
        }

        common_iterator operator++(int) {
            common_iterator copy(*this);
            ++*this;
            return copy;
        }

        common_iterator& operator--() {
            node = node->prev;
            return *this;
        }

        common_iterator operator--(int) {
            common_iterator copy(*this);
            --*this;
            return copy;
        }

        reference operator*() const {
            return node->value;
        }

        pointer operator->() const {
            return &node->value;
        }

        bool operator==(const common_iterator& other) const {
            return node == other.node;
        }

        bool operator!=(const common_iterator& other) const {
            return !(*this == other);
        }

        friend List;

    private:
        Node* node;

        void swap(common_iterator& other) {
            std::swap(node, other.node);
        }
    };

    void swap(List& other);

    Node* make_base_node() {
        auto ptr = NodeAllocTraits::allocate(node_alloc, 1);
        ptr->prev = ptr->next = nullptr;
        return ptr;
    }

    template<typename ...Args>
    Node* make_node(Args&&... args) {
        auto ptr = NodeAllocTraits::allocate(node_alloc, 1);
        NodeAllocTraits::construct(node_alloc, ptr, std::forward<Args>(args)...);
        return ptr;
    }

    void utilize_node(Node* node) {
        NodeAllocTraits::destroy(node_alloc, node);
        NodeAllocTraits::deallocate(node_alloc, node, 1);
    }

    void init_fictive_nodes();

    void insert_between(Node* left, Node* right, Node* middle);

    Node* extract(Node* node);
};

template<typename T, typename Alloc>
void List<T, Alloc>::swap(List& other) {
    std::swap(fictive_begin, other.fictive_begin);
    std::swap(fictive_end, other.fictive_end);
    std::swap(sz, other.sz);
    std::swap(alloc, other.alloc);
}

template<typename T, typename Alloc>
void List<T, Alloc>::init_fictive_nodes() {
    fictive_begin = make_base_node();
    fictive_end = make_base_node();
    fictive_begin->next = fictive_end;
    fictive_end->prev = fictive_begin;
}

template<typename T, typename Alloc>
void List<T, Alloc>::insert_between(List<T, Alloc>::Node* left, List<T, Alloc>::Node* right, List<T, Alloc>::Node* middle) {
    left->next = middle;
    right->prev = middle;
    middle->prev = left;
    middle->next = right;
}

template<typename T, typename Alloc>
typename List<T, Alloc>::Node* List<T, Alloc>::extract(List<T, Alloc>::Node* node) {
    auto left = node->prev;
    auto right = node->next;
    left->next = right;
    right->prev = left;
    node->next = node->prev = nullptr;
    return node;
}
