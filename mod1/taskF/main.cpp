/* Задача F. Инверсии
Напишите программу, которая для заданного массива A=⟨a1,a2,…,an⟩ находит количество пар (i,j)
таких, что i<j и a_i>a_j. Обратите внимание на то, что ответ может не влезать в int.

Входные данные

Первая строка входного файла содержит натуральное число n (1⩽n⩽100000) — количество элементов
массива. Вторая строка содержит n попарно различных элементов массива A — целых неотрицательных
чисел, не превосходящих 1e9.

Выходные данные

В выходной файл выведите одно число — ответ на задачу.
*/

#include <iostream>
#include <vector>

using std::vector;

void merge(vector<int> &res, const vector<int> &left, const vector<int> &right, long long &inv_count) {
    res.clear();
    int pos1 = 0, pos2 = 0;
    while (pos1 < left.size() && pos2 < right.size()) {
        if (left[pos1] <= right[pos2]) {
            res.emplace_back(left[pos1]);
            ++pos1;
        } else {
            res.emplace_back(right[pos2]);
            ++pos2;
            inv_count += left.size() - pos1;  // кол-во элементов, которые ещё не были
            // рассмотрены в left (т.е. будут стоять после right[pos2] => все составляют инверсии
            // с right[pos2] в изначальном массиве)
        }
    }
    for (; pos1 < left.size(); ++pos1) {
        res.emplace_back(left[pos1]);
    }
    for (; pos2 < right.size(); ++pos2) {
        res.emplace_back(right[pos2]);
    }
}

void merge_sort(vector<int> &arr, long long &inv_count) {
    if (arr.size() <= 1) {
        return;
    }
    vector<int> first_half, second_half;
    for (int i = 0; i < arr.size(); ++i) {
        if (i < arr.size() / 2) {
            first_half.emplace_back(arr[i]);
        } else {
            second_half.emplace_back(arr[i]);
        }
    }
    merge_sort(first_half, inv_count);
    merge_sort(second_half, inv_count);
    merge(arr, first_half, second_half, inv_count);
}

void enable_fast_io() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
}

int main() {
    using std::cin, std::cout, std::endl;
    enable_fast_io();

    freopen("inverse.in", "r", stdin);
    freopen("inverse.out", "w", stdout);

    int n;
    cin >> n;
    vector<int> arr(n);
    for (int &x : arr) cin >> x;

    long long inv_count = 0;
    merge_sort(arr, inv_count);

    cout << inv_count << endl;

    return 0;
}
