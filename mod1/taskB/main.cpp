/* Задача B. Правильная скобочная последовательность
По заданной строке из круглых/квадратных/фигурных открывающих и закрывающих скобок определить,
является ли она правильной скобочной последовательностью.

Множество правильных скобочных последовательностей (ПСП) определяется как наименьшее множество
с условиями:
    пустая строка является ПСП;
    если S — ПСП, то (S), [S], {S} — тоже ПСП;
    если S_1 и S_2 — ПСП, то S_1S_2 — тоже ПСП.

* Входные данные

В единственной строке содержится последовательность из круглых/квадратных/фигурных открывающих
и закрывающих скобок.

* Выходные данные

Выведите «yes», если введённая строка является правильной скобочной последовательностью, и
«no» иначе.
*/

#include <iostream>
#include <cassert>
#include <string>

struct Node {
    int value;
    Node* next;
};


// стек абсолютно такой же, как в задаче А
struct Stack {
    Node* last = nullptr;
    int size = 0;

    bool is_empty() {
        return last == nullptr;
    }

    void push(int val) {
        Node* new_node = new Node();
        new_node->value = val;
        new_node->next = last;
        last = new_node;
        ++size;
    }

    int back() {
        assert(!is_empty());
        return last->value;
    }

    int pop() {
        assert(!is_empty());
        int last_value = last->value;
        Node* cur_last = last;
        last = last->next;
        free(cur_last);
        --size;
        return last_value;
    }

    void clear() {
        Node* cur = last;
        while (cur != nullptr) {
            Node* nxt = cur->next;
            delete cur;
            cur = nxt;
        }
        last = nullptr;
        size = 0;
    }
};

char opposite(char bracket) {
    switch (bracket) {
        case ']':
            return '[';
        case '}':
            return '{';
        case ')':
            return '(';
    }
}

bool is_correct_seq(const std::string &s) {
    Stack stack;

    for (char c : s) {
        switch (c) {
            case '(':
            case '[':
            case '{':
                stack.push(c);
                break;
            case ')':
            case ']':
            case '}':
                if (!stack.is_empty() && stack.back() == opposite(c)) {
                    stack.pop();
                } else {
                    stack.clear();
                    return false;
                }
        }
    }
    if (stack.is_empty()) {
        stack.clear();
        return true;
    } else {
        stack.clear();
        return false;
    }
}

int main() {
    using std::cin, std::cout, std::endl;

    std::string s;
    cin >> s;
    if (is_correct_seq(s)) {
        cout << "yes" << endl;
    } else {
        cout << "no" << endl;
    }

    return 0;
}
